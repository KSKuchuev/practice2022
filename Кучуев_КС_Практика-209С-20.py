import matplotlib.pyplot as plt
from math import * 


sat_num = input("Введите номер спутника в формате PC01 ")


count_lines=0

#Чтение данных из файла Sta30s22022.sp3 и подсчет колличества строк с описанием положения в пространстве 
file = open("/home/pomidorka/.vscode/extensions/practice/Sta30s22022.sp3", 'r') 
for str in file.readlines():
    
    if ((str[0] == sat_num[0]) and (str[1] == sat_num[1]) and (str[2] == sat_num[2]) and (str[3] == sat_num[3])):
        count_lines+=1  

file.seek(0)


class satellite:
    #Координаты в подвижной системе координат
    x = [i for i in range(count_lines)]
    y = [i for i in range(count_lines)]
    z = [i for i in range(count_lines)]
    t = [i for i in range(count_lines)]
    #Координаты в неподвижной системе координат
    xgroung = [i for i in range(count_lines)]
    yground = [i for i in range(count_lines)]
    time = [i for i in range(count_lines)]
    

# Парсинг
Iteration = 0

for str in file.readlines():
    
    # Парсинг по первоначальным опредедлителям координат 
    if ((str[0] == sat_num[0]) and (str[1] == sat_num[1]) and (str[2] == sat_num[2]) and (str[3] == sat_num[3])):
        Place = 0
        
        l = str.split()
        for i in range(0, len(l)):
            
            Flag = 1
            # Проверка текста на возможные ошибки
            if(len(l[i]) == 0):
                Flag = 0
            else:
                for j in range(0, len(l[i])):
                    if(l[i][j].isalpha()):
                        Flag = 0
                        break
            # Чтение
            if(Flag == 1):                 
                match Place:
                    case 0:
                        satellite.x[Iteration] = float(l[i])
                        Place = 1
                    case 1:
                        satellite.y[Iteration] = float(l[i])
                        Place = 2
                    case 2:
                        satellite.z[Iteration] = float(l[i])
                        Place = 3
                    case _:
                        satellite.t[Iteration] = float(l[i])
                        Iteration += 1  

                         
file.close()


#Переход между системами координат
angle = 0.002 * pi / 180 # с учетом угловой скорости в 7,27×10-5 рад/с за 30 секунд обновления данных

for i in range(0, count_lines):
    satellite.xgroung[i] = satellite.x[i]  * cos(angle) - satellite.y[i] * sin(angle)
    satellite.yground[i] = satellite.y[i]  * cos(angle) + satellite.x[i] * sin(angle)


#Подсчёт времени
for i in range(0, count_lines):
    satellite.time[i] = i * 30



#Построение графиков
plt.subplot(1, 3, 1)
plt.plot(satellite.xgroung, satellite.yground)
plt.title("Траектория спутника ") 
plt.xlabel('X , км')  
plt.ylabel('Y , км') 

plt.subplot(1, 3, 2)
plt.plot(satellite.time, satellite.xgroung) 
plt.title("Положение Х от времени в секундах ") 
plt.ylabel('Х , км')
plt.xlabel('время в секундах') 

plt.subplot(1, 3, 3)
plt.plot(satellite.time, satellite.yground) 
plt.title("Положение Y от времени в секундах ") 
plt.ylabel('Y , км')
plt.xlabel('время в секундах') 

plt.show() 